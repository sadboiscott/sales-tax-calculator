﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface INexusAddressBuilder {
        public INexusAddressBuilder WithId(string id);

        public INexusAddressBuilder WithCountryCode(string country);

        public INexusAddressBuilder WithPostalCode(string postal);

        public INexusAddressBuilder WithStateCode(string state);

        public INexusAddressBuilder WithCity(string city);

        public INexusAddressBuilder WithStreetAddress(string address);

        public Either<ErrorList, NexusAddress> Build();
    }
}
