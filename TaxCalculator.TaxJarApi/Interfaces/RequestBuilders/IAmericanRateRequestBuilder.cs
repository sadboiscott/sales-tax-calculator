﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface IAmericanRateRequestBuilder {
        public IAmericanRateRequestBuilder WithOriginCountryCode(string country);

        public IAmericanRateRequestBuilder WithOriginPostalCode(string postal);

        public IAmericanRateRequestBuilder WithOriginCity(string city);

        public IAmericanRateRequestBuilder WithOriginStateCode(string state);

        public IAmericanRateRequestBuilder WithOriginStreetAddress(string street);

        public IAmericanRateRequestBuilder WithDestinationCountryCode(string country);

        public IAmericanRateRequestBuilder WithDestinationPostalCode(string postal);

        public IAmericanRateRequestBuilder WithDestinationCity(string city);

        public IAmericanRateRequestBuilder WithDestinationStateCode(string state);

        public IAmericanRateRequestBuilder WithDestinationStreetAddress(string street);

        public IAmericanRateRequestBuilder WithOrderTotal(decimal amount);

        public IAmericanRateRequestBuilder WithShipping(decimal amount);

        public IAmericanRateRequestBuilder WithCustomerId(string id);

        public IAmericanRateRequestBuilder WithExemption(string exemption);

        public IAmericanRateRequestBuilder AddNexus(NexusAddress nexus);

        public IAmericanRateRequestBuilder AddLineItem(LineItem lineItem);

        public IAmericanRateRequestBuilder Reset();

        public ILineItemBuilder LineItemBuilder { get; }

        public INexusAddressBuilder NexusBuilder { get; }

        public Either<ErrorList, ApiCalculationRequest> Build();
    }
}
