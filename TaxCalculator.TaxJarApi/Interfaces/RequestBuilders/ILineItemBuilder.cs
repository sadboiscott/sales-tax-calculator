﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface ILineItemBuilder {
        public ILineItemBuilder WithId(string id);

        public ILineItemBuilder WithQuantity(int quantity);

        public ILineItemBuilder WithProductTaxCode(string taxCode);

        public ILineItemBuilder UnitPrice(decimal unitPrice);

        public ILineItemBuilder NonUnitDiscount(decimal discount);

        public Either<ErrorList, LineItem> Build();
    }
}
