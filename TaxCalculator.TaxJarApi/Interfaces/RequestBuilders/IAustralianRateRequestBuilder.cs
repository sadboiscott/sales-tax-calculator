﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface IAustralianRateRequestBuilder {
        public IAustralianRateRequestBuilder WithOriginCountryCode(string country);

        public IAustralianRateRequestBuilder WithOriginPostalCode(string postal);

        public IAustralianRateRequestBuilder WithOriginCity(string city);

        public IAustralianRateRequestBuilder WithOriginStateCode(string state);

        public IAustralianRateRequestBuilder WithOriginStreetAddress(string street);

        internal IAustralianRateRequestBuilder WithDestinationCountryCode(string country);

        public IAustralianRateRequestBuilder WithDestinationPostalCode(string postal);

        public IAustralianRateRequestBuilder WithDestinationCity(string city);

        public IAustralianRateRequestBuilder WithDestinationStateCode(string state);

        public IAustralianRateRequestBuilder WithDestinationStreetAddress(string street);

        public IAustralianRateRequestBuilder WithOrderTotal(decimal amount);

        public IAustralianRateRequestBuilder WithShipping(decimal amount);

        public IAustralianRateRequestBuilder WithCustomerId(string id);

        public IAustralianRateRequestBuilder WithExemption(string exemption);

        public IAustralianRateRequestBuilder AddNexus(NexusAddress nexus);

        public IAustralianRateRequestBuilder AddLineItem(LineItem lineItem);

        public IAustralianRateRequestBuilder Reset();

        public ILineItemBuilder LineItemBuilder { get; }

        public INexusAddressBuilder NexusBuilder { get; }

        public Either<ErrorList, ApiCalculationRequest> Build();
    }
}
