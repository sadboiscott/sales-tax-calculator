﻿namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface IRateRequestBuilder {
        public IAmericanRateRequestBuilder ForAmerica();

        public IAustralianRateRequestBuilder ForAustralia();

        public ICanadianRateRequestBuilder ForCanada();

        public IEuropeanRateRequestBuilder ForEurope();
    }
}
