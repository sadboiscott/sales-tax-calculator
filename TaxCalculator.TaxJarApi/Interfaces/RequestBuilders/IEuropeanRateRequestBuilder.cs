﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Interfaces.RequestBuilders {
    public interface IEuropeanRateRequestBuilder {
        public IEuropeanRateRequestBuilder WithOriginCountryCode(string country);

        public IEuropeanRateRequestBuilder WithOriginPostalCode(string postal);

        public IEuropeanRateRequestBuilder WithOriginCity(string city);

        public IEuropeanRateRequestBuilder WithOriginStateCode(string state);

        public IEuropeanRateRequestBuilder WithOriginStreetAddress(string street);

        public IEuropeanRateRequestBuilder WithDestinationCountryCode(string country);

        public IEuropeanRateRequestBuilder WithDestinationPostalCode(string postal);

        public IEuropeanRateRequestBuilder WithDestinationCity(string city);

        public IEuropeanRateRequestBuilder WithDestinationStateCode(string state);

        public IEuropeanRateRequestBuilder WithDestinationStreetAddress(string street);

        public IEuropeanRateRequestBuilder WithOrderTotal(decimal amount);

        public IEuropeanRateRequestBuilder WithShipping(decimal amount);

        public IEuropeanRateRequestBuilder WithCustomerId(string id);

        public IEuropeanRateRequestBuilder WithExemption(string exemption);

        public IEuropeanRateRequestBuilder AddNexus(NexusAddress nexus);

        public IEuropeanRateRequestBuilder AddLineItem(LineItem lineItem);

        public IEuropeanRateRequestBuilder Reset();

        public ILineItemBuilder LineItemBuilder { get; }

        public INexusAddressBuilder NexusBuilder { get; }

        public Either<ErrorList, ApiCalculationRequest> Build();
    }
}
