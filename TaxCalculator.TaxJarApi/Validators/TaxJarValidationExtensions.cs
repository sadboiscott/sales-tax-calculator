﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Models;
using TaxCalculator.Core.Validation;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

namespace TaxCalculator.TaxJarApi.Validators {
    public static class TaxJarValidationExtensions {
        private static readonly IEnumerable<string> ExemptionTypes = new[] {
                                                                               "wholesale",
                                                                               "government",
                                                                               "marketplace",
                                                                               "other",
                                                                               "non_exempt"
                                                                           };

        private static readonly IEnumerable<string> SupportedEUCountries = new [] {
                                                                                CountryCodes.Austria,
                                                                                   CountryCodes.Belgium,
                                                                                   CountryCodes.Bulgaria,
                                                                                   CountryCodes.Croatia,
                                                                                   CountryCodes.Cyprus,
                                                                                   CountryCodes.Denmark,
                                                                                   CountryCodes.Estonia,
                                                                                   CountryCodes.Finland,
                                                                                   CountryCodes.France,
                                                                                   CountryCodes.Germany,
                                                                                   CountryCodes.Greece,
                                                                                   CountryCodes.Hungary,
                                                                                   CountryCodes.Ireland,
                                                                                   CountryCodes.Italy,
                                                                                   CountryCodes.Latvia,
                                                                                   CountryCodes.Lithuania,
                                                                                   CountryCodes.Luxembourg,
                                                                                   CountryCodes.Malta,
                                                                                   CountryCodes.Netherlands,
                                                                                   CountryCodes.Poland,
                                                                                   CountryCodes.Portugal,
                                                                                   CountryCodes.Romania,
                                                                                   CountryCodes.Slovakia,
                                                                                   CountryCodes.Slovenia,
                                                                                   CountryCodes.Spain,
                                                                                   CountryCodes.Sweden,
                                                                                   CountryCodes.CzechRepublic,
                                                                                   CountryCodes.UnitedKingdom
                                                                           };

        private static readonly IEnumerable<string> SupportedCountries = new List<string>(SupportedEUCountries) {
                                                                             CountryCodes.America,
                                                                             CountryCodes.Australia,
                                                                             CountryCodes.Canada
                                                                         };

        public static IRuleBuilderOptions<T, ApiCalculationRequest> MustHaveAmountPresent<T>(
            this IRuleBuilder<T, ApiCalculationRequest> builder) =>
            builder.Must(x =>
                    x.Amount != default || x.LineItems != null && x.LineItems.Any())
                .WithMessage(ErrorMessages.CalculationRequestMustHaveAmountSet)
                .WithErrorCode(ErrorCodes.CalculationMustNotHaveTwoAmountsSet);

        public static IRuleBuilderOptions<T, ApiCalculationRequest> MustNotHaveBothAmountsSet<T>(
            this IRuleBuilder<T, ApiCalculationRequest> builder) =>
            builder.Must(x =>
                    x.Amount != default ^ (x.LineItems != null && x.LineItems.Any()))
                .WithMessage(ErrorMessages.CalculationRequestMustNotHaveBothAmountsSet)
                .WithErrorCode(ErrorCodes.CalculationMustNotHaveTwoAmountsSet);

        public static IRuleBuilderOptions<T, string> MustBeExemptionType<T>(this IRuleBuilder<T, string> builder) =>
            builder.Must(x => ExemptionTypes
                    .Any(exempt =>
                        exempt.Equals(x,
                            StringComparison.InvariantCultureIgnoreCase)))
                .WithMessage(ErrorMessages.CalculationRequestExemptionNotFound)
                .WithErrorCode(ErrorCodes.CalculationExemptionTypeNotFound);

        public static IRuleBuilderOptions<T, string>
            TaxJarMustSupportCountry<T>(this IRuleBuilder<T, string> builder) =>
            builder.Must(x => SupportedCountries.Any(sc =>
                    sc.Equals(x,
                        StringComparison.InvariantCultureIgnoreCase)))
                .WithMessage(ErrorMessages.TaxJarUnsupportedCountry)
                .WithErrorCode(ErrorCodes.TaxJarApiUnsupportedCountry);

        public static IRuleBuilderOptions<LineItem, decimal> LineItemDiscountCannotExceedPrice(
            this IRuleBuilder<LineItem, decimal> builder) =>
            builder.Must((root, discount, context) => {
                    context.MessageFormatter.AppendArgument("Price", root.UnitPrice);
                    context.MessageFormatter.AppendArgument("Discount", discount);

                    return discount <= root.UnitPrice;
                })
                .WithMessage(ErrorMessages.LineItemDiscountCannotExceedPrice)
                .WithErrorCode(ErrorCodes.LineItemDiscountCannotExceedUnitPrice);

        public static IRuleBuilderOptions<ApiCalculationRequest, string> MustBeToStateIfNorthAmerican(
            this IRuleBuilder<ApiCalculationRequest, string> builder) =>
            builder.Must((root, str, context) => {
                    if (string.IsNullOrEmpty(root.ToCountryCode)) {
                        return false;
                    }

                    if (root.ToCountryCode.Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase)
                        || root.ToCountryCode.Equals(CountryCodes.America,
                            StringComparison.InvariantCultureIgnoreCase)) {
                        return !string.IsNullOrEmpty(root.ToStateCode) && root.ToStateCode.Length == 2;
                    }

                    return true;
                })
                .WithMessage(ErrorMessages.ToStateNotValidIsoCode)
                .WithErrorCode(ErrorCodes.ToStateNotValidIsoCode);

        public static IRuleBuilderOptions<ApiCalculationRequest, string> MustBeFromStateIfNorthAmerican(
            this IRuleBuilder<ApiCalculationRequest, string> builder) =>
            builder.Must((root, str, context) => {
                    if (string.IsNullOrEmpty(root.FromCountryCode)) {
                        return false;
                    }

                    if (root.FromCountryCode.Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase)
                        || root.FromCountryCode.Equals(CountryCodes.America,
                            StringComparison.InvariantCultureIgnoreCase)) {
                        return !string.IsNullOrEmpty(root.FromStateCode) && root.FromStateCode.Length == 2;
                    }

                    return true;
                })
                .WithMessage(ErrorMessages.FromStateNotValidIsoCode)
                .WithErrorCode(ErrorCodes.FromStateNotValidIsoCode);

        public static bool IsSupportedEuropeanCountry(this string country) =>
            SupportedEUCountries.Any(x => x.Equals(country, StringComparison.InvariantCultureIgnoreCase));

        public static IRuleBuilderOptions<ApiCalculationRequest, string> MustBeValidPostalCodeFromCountry(
            this IRuleBuilder<ApiCalculationRequest, string> builder) =>
            builder.Must(
                    (root, str, context) =>
                        !(string.IsNullOrEmpty(str) || string.IsNullOrEmpty(root.FromCountryCode)))
                .WithMessage(ErrorMessages.StringNullOrEmptyMessage)
                .WithErrorCode(ErrorCodes.StringNullOrEmptyError)
                .Must((root, str, context) => root.FromCountryCode.Equals(CountryCodes.Australia) switch {
                    true => !string.IsNullOrEmpty(str)&& str.IsAustralianPostCode(),
                    _ => true
                })
                .Unless(x => string.IsNullOrEmpty(x.FromCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotAustralian)
                .WithErrorCode(ErrorCodes.PostCodeNotAustralian)
                .Must(
                    (root, str, context) =>
                        root.FromCountryCode.Equals(CountryCodes.America,
                            StringComparison.InvariantCultureIgnoreCase) switch {
                            true => !string.IsNullOrEmpty(str) && str.IsAmericanPostCode(),
                            _ => true
                        }
                    )
                .Unless(x => string.IsNullOrEmpty(x.FromCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotAmerican)
                .WithErrorCode(ErrorCodes.PostCodeNotAmerican)
                .Must(
                    (root, str, context) => root.FromCountryCode
                        .Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase) switch {
                        true => !string.IsNullOrEmpty(str) && str.IsCanadianPostCode(),
                        _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.FromCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotCanadian)
                .WithErrorCode(ErrorCodes.PostCodeNotCanadian)
                .Must(
                    (root, str, context) => root.FromCountryCode
                        .Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase) switch {
                        true => !string.IsNullOrEmpty(str) ,
                        _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.FromCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotCanadian)
                .WithErrorCode(ErrorCodes.PostCodeNotCanadian)
                .Must((root, str, context) => root.FromCountryCode.IsSupportedEuropeanCountry() switch {
                    true => !string.IsNullOrEmpty(str),
                    _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.FromCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotEuropean)
                .WithErrorCode(ErrorCodes.PostCodeNotEuropean);

        public static IRuleBuilderOptions<ApiCalculationRequest, string> MustBeValidPostalCodeToCountry(
            this IRuleBuilder<ApiCalculationRequest, string> builder) =>
            builder.Must(
                    (root, str, context) =>
                        !(string.IsNullOrEmpty(str) || string.IsNullOrEmpty(root.ToCountryCode)))
                .WithMessage(ErrorMessages.StringNullOrEmptyMessage)
                .WithErrorCode(ErrorCodes.StringNullOrEmptyError)
                .Must((root, str, context) => root.ToCountryCode.Equals(CountryCodes.Australia) switch {
                    true => !string.IsNullOrEmpty(str)&& str.IsAustralianPostCode(),
                    _ => true
                })
                .Unless(x => string.IsNullOrEmpty(x.ToCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotAustralian)
                .WithErrorCode(ErrorCodes.PostCodeNotAustralian)
                .Must(
                    (root, str, context) =>
                        root.ToCountryCode.Equals(CountryCodes.America,
                            StringComparison.InvariantCultureIgnoreCase) switch {
                            true => !string.IsNullOrEmpty(str) && str.IsAmericanPostCode(),
                            _ => true
                        }
                    )
                .Unless(x => string.IsNullOrEmpty(x.ToCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotAmerican)
                .WithErrorCode(ErrorCodes.PostCodeNotAmerican)
                .Must(
                    (root, str, context) => root.ToCountryCode
                        .Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase) switch {
                        true => !string.IsNullOrEmpty(str) && str.IsCanadianPostCode(),
                        _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.ToCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotCanadian)
                .WithErrorCode(ErrorCodes.PostCodeNotCanadian)
                .Must(
                    (root, str, context) => root.ToCountryCode
                        .Equals(CountryCodes.Canada, StringComparison.InvariantCultureIgnoreCase) switch {
                        true => !string.IsNullOrEmpty(str) ,
                        _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.ToCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotCanadian)
                .WithErrorCode(ErrorCodes.PostCodeNotCanadian)
                .Must((root, str, context) => root.ToCountryCode.IsSupportedEuropeanCountry() switch {
                    true => !string.IsNullOrEmpty(str),
                    _ => true
                    })
                .Unless(x => string.IsNullOrEmpty(x.ToCountryCode))
                .WithMessage(ErrorMessages.PostalCodeNotEuropean)
                .WithErrorCode(ErrorCodes.PostCodeNotEuropean);
    }
}
