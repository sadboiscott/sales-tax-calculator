﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Interfaces.RequestBuilders;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class NexusAddressBuilder : INexusAddressBuilder {
        private string _id;
        private string _country;
        private string _state;
        private string _city;
        private string _street;
        private string _postal;

        public INexusAddressBuilder WithId(string id) {
            _id = id;

            return this;
        }

        public INexusAddressBuilder WithCountryCode(string country) {
            _country = country;

            return this;
        }

        public INexusAddressBuilder WithPostalCode(string postal) {
            _postal = postal;

            return this;
        }

        public INexusAddressBuilder WithStateCode(string state) {
            _state = state;

            return this;
        }

        public INexusAddressBuilder WithCity(string city) {
            _city = city;

            return this;
        }

        public INexusAddressBuilder WithStreetAddress(string address) {
            _street = address;

            return this;
        }

        public Either<ErrorList, NexusAddress> Build() {
            var nexus = new NexusAddress() {
                                               Id = _id,
                                               City = _city,
                                               CountryCode = _country,
                                               PostalCode = _postal,
                                               StateCode = _state,
                                               StreetAddress = _street
                                           };

            return Either.Right<ErrorList, NexusAddress>(nexus);
        }
    }
}
