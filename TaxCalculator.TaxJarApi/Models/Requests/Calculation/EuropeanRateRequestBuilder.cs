﻿using System.Collections.Generic;
using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Interfaces.RequestBuilders;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class EuropeanRateRequestBuilder : IEuropeanRateRequestBuilder {
        private string _originCountry;

        private string _originPostal;

        private string _originCity;

        private string _originState;

        private string _originStreet;

        private string _destinationCountry;

        private string _destinationPostal;

        private string _destinationCity;

        private string _destinationState;

        private string _destinationAddress;

        private decimal _orderAmount;

        private decimal _shippingAmount;

        private string _customerId;

        private string _exemption;

        internal List<LineItem> LineItems;

        internal List<NexusAddress> NexusLocations;

        private readonly ILineItemBuilder _lineItemBuilder;

        private readonly INexusAddressBuilder _nexusBuilder;

        public IEnumerable<LineItem> LineItemsCollection => LineItems;

        public IEnumerable<NexusAddress> NexusAddresses => NexusLocations;

        public IEuropeanRateRequestBuilder WithOriginCountryCode(string country) {
            _originCountry = country;

            return this;
        }

        public IEuropeanRateRequestBuilder WithOriginPostalCode(string postal) {
            _originPostal = postal;

            return this;
        }

        public IEuropeanRateRequestBuilder WithOriginCity(string city) {
            _originCity = city;

            return this;
        }

        public IEuropeanRateRequestBuilder WithOriginStateCode(string state) {
            _originState = state;

            return this;
        }

        public IEuropeanRateRequestBuilder WithOriginStreetAddress(string street) {
            _originStreet = street;

            return this;
        }

        public IEuropeanRateRequestBuilder WithDestinationCountryCode(string country) {
            _destinationCountry = country;

            return this;
        }

        public IEuropeanRateRequestBuilder WithDestinationPostalCode(string postal) {
            _destinationPostal = postal;

            return this;
        }

        public IEuropeanRateRequestBuilder WithDestinationCity(string city) {
            _destinationCity = city;

            return this;
        }

        public IEuropeanRateRequestBuilder WithDestinationStateCode(string state) {
            _destinationState = state;

            return this;
        }

        public IEuropeanRateRequestBuilder WithDestinationStreetAddress(string street) {
            _destinationAddress = street;

            return this;
        }

        public IEuropeanRateRequestBuilder WithOrderTotal(decimal amount) {
            _orderAmount = amount;

            return this;
        }

        public IEuropeanRateRequestBuilder WithShipping(decimal amount) {
            _shippingAmount = amount;

            return this;
        }

        public IEuropeanRateRequestBuilder WithCustomerId(string id) {
            _customerId = id;

            return this;
        }

        public IEuropeanRateRequestBuilder WithExemption(string exemption) {
            _exemption = exemption;

            return this;
        }

        public IEuropeanRateRequestBuilder AddNexus(NexusAddress nexus) {
            NexusLocations.Add(nexus);

            return this;
        }

        public IEuropeanRateRequestBuilder AddLineItem(LineItem lineItem) {
            LineItems.Add(lineItem);

            return this;
        }

        public IEuropeanRateRequestBuilder Reset() {
            _originCity = default;
            _originCountry = default;
            _originPostal = default;
            _originState = default;
            _originStreet = default;
            _destinationAddress = default;
            _destinationCity = default;
            _destinationCountry = default;
            _destinationPostal = default;
            _destinationState = default;
            _orderAmount = default;
            _shippingAmount = default;
            _customerId = default;
            _exemption = default;
            LineItems = new List<LineItem>();
            NexusLocations = new List<NexusAddress>();

            return this;
        }

        public ILineItemBuilder LineItemBuilder => _lineItemBuilder;

        public INexusAddressBuilder NexusBuilder => _nexusBuilder;

        public Either<ErrorList, ApiCalculationRequest> Build() {
            var request = new ApiCalculationRequest {
                                                        Amount = _orderAmount,
                                                        CustomerId = _customerId,
                                                        ExemptionType = _exemption,
                                                        FromCity = _originCity,
                                                        FromCountryCode = _originCountry,
                                                        FromPostalCode = _originPostal,
                                                        FromStateCode = _originState,
                                                        FromStreetAddress = _originStreet,
                                                        ToCity = _destinationCity,
                                                        ToPostalCode = _destinationPostal,
                                                        ToCountryCode = _destinationCountry,
                                                        ToStateCode = _destinationState,
                                                        ToStreetAddress = _destinationAddress,
                                                        TotalShipping = _shippingAmount,
                                                        NexusLocations = new List<NexusAddress>(NexusLocations),
                                                        LineItems = new List<LineItem>(LineItems)
                                                    };

            return Either.Right<ErrorList, ApiCalculationRequest>(request);
        }

        public EuropeanRateRequestBuilder(ILineItemBuilder lineItemBuilder,
            INexusAddressBuilder nexusBuilder) {
            _lineItemBuilder = lineItemBuilder;
            _nexusBuilder = nexusBuilder;

            LineItems = new List<LineItem>();
            NexusLocations = new List<NexusAddress>();
        }
    }
}
