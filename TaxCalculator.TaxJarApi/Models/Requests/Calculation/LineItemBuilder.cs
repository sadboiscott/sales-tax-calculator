﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.TaxJarApi.Interfaces.RequestBuilders;

namespace TaxCalculator.TaxJarApi.Models.Requests.Calculation {
    public class LineItemBuilder : ILineItemBuilder {
        private string _id;

        private int _quantity;

        private string _taxCode;

        private decimal _unitPrice;

        private decimal _discount;

        public ILineItemBuilder WithId(string id) {
            _id = id;

            return this;
        }

        public ILineItemBuilder WithQuantity(int quantity) {
            _quantity = quantity;

            return this;
        }

        public ILineItemBuilder WithProductTaxCode(string taxCode) {
            _taxCode = taxCode;

            return this;
        }

        public ILineItemBuilder UnitPrice(decimal unitPrice) {
            _unitPrice = unitPrice;

            return this;
        }

        public ILineItemBuilder NonUnitDiscount(decimal discount) {
            _discount = discount;

            return this;
        }

        public Either<ErrorList, LineItem> Build() {
            var lineItem = new LineItem() {
                                              Id = _id,
                                              NonUnitItemDiscount = _discount,
                                              ProductTaxCode =  _taxCode,
                                              Quantity = _quantity,
                                              UnitPrice = _unitPrice
                                          };

            return Either.Right<ErrorList, LineItem>(lineItem);
        }
    }
}
