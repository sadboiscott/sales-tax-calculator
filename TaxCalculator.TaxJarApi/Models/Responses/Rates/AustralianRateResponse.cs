﻿using System.Text.Json.Serialization;

namespace TaxCalculator.TaxJarApi.Models.Responses.Rates {
    public class AustralianRateResponse : ApiTaxRatesResponse {
        [JsonPropertyName("country_rate")]
        public string CountryRate { get; private set; }

        /// <summary>
        /// Use this value for calculating taxes on an order manually instead
        /// of calling the API's calculation endpoint.
        /// </summary>
        [JsonPropertyName("combined_rate")]
        public string CombinedRate { get; private set; }
    }
}
