﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.Core.Interfaces;
using TaxCalculator.Core.Models;

namespace TaxCalculator.TaxJarApi.Service {
    public class TaxCalculator : ITaxCalculator {
        public Either<ErrorList, CalculationResponse> CalculateTaxesOnOrder(CalculationRequest request) => default;

        public Either<ErrorList, LocationTaxesResponse> GetTaxesForLocation(LocationTaxesRequest request) => default;
    }
}
