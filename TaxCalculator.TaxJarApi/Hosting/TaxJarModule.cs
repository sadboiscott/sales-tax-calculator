﻿using System.Runtime.CompilerServices;
using Autofac;
using Autofac.Features.AttributeFilters;
using TaxCalculator.Core.Interfaces;
using TaxCalculator.TaxJarApi.Interfaces.RequestBuilders;
using TaxCalculator.TaxJarApi.Models.Requests.Calculation;

[assembly: InternalsVisibleTo("TaxCalculator.TaxJarApi.Tests")]

namespace TaxCalculator.TaxJarApi.Hosting {
    public class TaxJarModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<Service.TaxCalculator>()
                .As<ITaxCalculator>()
                .WithAttributeFiltering()
                .InstancePerLifetimeScope();

            builder.RegisterType<LineItemBuilder>()
                .As<ILineItemBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<NexusAddressBuilder>()
                .As<INexusAddressBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AmericanRateRequestBuilder>()
                .As<IRateRequestBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AustralianRateRequestBuilder>()
                .As<IAustralianRateRequestBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CanadianRateRequestBuilder>()
                .As<ICanadianRateRequestBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EuropeanRateRequestBuilder>()
                .As<IEuropeanRateRequestBuilder>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestBuilder>()
                .As<IRateRequestBuilder>()
                .InstancePerLifetimeScope();
        }
    }
}
