﻿using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using TaxCalculator.Core.Config;
using TaxCalculator.Core.Hosting;
using TaxCalculator.Core.Models;
using TaxCalculator.TaxJarApi.Hosting;
using TaxCalculator.TaxJarApi.Validators.Request.Calculation;

namespace TaxCalculator {
    public class Startup {
        public IConfigurationRoot Configuration { get; private set; }

        public Startup(IHostEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", reloadOnChange: true, optional: false)
                .AddJsonFile($"appsettings.{env.EnvironmentName}", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddOptions();
            services.AddLogging();
            services.AddHttpClient();
            services.AddAutofac();
            services.Configure<CacheConfiguration>(Configuration.GetSection(CacheConfiguration.SectionKey));
            services.AddValidatorsFromAssemblyContaining<ApiRequestValidator>();
            services.AddValidatorsFromAssemblyContaining<Address>();
        }

        public void ConfigureContainer(ContainerBuilder builder) {
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new TaxJarModule());
        }
    }
}
