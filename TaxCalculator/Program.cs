﻿using System;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace TaxCalculator {
    class Program {
        static async Task Main(string[] args) {
            Log.Logger = Serilog;

            try {
                Log.Information("Application has been started. Initializing...");
                await CreateHostBuilder(args).Build().RunAsync();
            }
            catch (Exception e) {
                Log.Fatal(e, "Application initialization terminated unexpectedly.");
            }
            finally {
                Log.CloseAndFlush();
            }
        }

        private static Logger Serilog = new LoggerConfiguration()
            .ReadFrom.Configuration(new ConfigurationBuilder().Build())
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .MinimumLevel.Override("System", LogEventLevel.Information)
            .CreateLogger();

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .UseConsoleLifetime()
                .UseSerilog(Serilog);
    }
}
