﻿using System;
using FluentAssertions;
using TaxCalculator.Core.Functors;
using TaxCalculator.Core.Models;
using Xunit;

namespace TaxCalculator.Core.Tests.Maybe {
    public class MaybeFunctorLaws {
        /// <summary>
        /// The first law of functors dictates that mapping the identity function
        /// returns the original input value unchanged.
        /// </summary>
        [Theory]
        [InlineData("")]
        [InlineData("abc")]
        [InlineData("abc123")]
        [InlineData("floccinaucinihilipilification")]
        public void Maybe_ObeysFirstFunctorLaw_Success(string value) {
            Func<string, string> id = x => x;
            var sut = new Maybe<string>(value);

            Assert.Equal(sut, sut.Select(id));
        }

        /// <summary>
        /// Verifies that the first law of functors still holds when no value
        /// is stored by a Maybe functor.
        /// </summary>
        [Fact]
        public void Maybe_ObeysFirstFunctorLaw_WhenEmpty() {
            Func<string, string> id = x => x;
            var sut = new Maybe<string>();

            Assert.Equal(sut, sut.Select(id));
        }

        /// <summary>
        /// The second law of functors states that given two functions, f and g,
        /// mapping one after the other should be equivalent to the mapping
        /// produced over the composition of f and g.
        /// </summary>
        [Theory]
        [InlineData("")]
        [InlineData("abc")]
        [InlineData("abc123")]
        [InlineData("floccinaucinihilipilification")]
        public void Maybe_ObeysSecondFunctorLaw_Success(string value) {
            Func<string, int> g = s => s.Length;
            Func<int, bool> f = i => i % 2 == 0;
            var sut = new Maybe<string>(value);

            Assert.Equal(sut.Select(g).Select(f), sut.Select(s => f(g(s))));
        }


        [Fact]
        public void Maybe_ObeysSecondFunctorLaw_WhenEmpty() {
            Func<string, int> g = s => s.Length;
            Func<int, bool> f = i => i % 2 == 0;
            var sut = new Maybe<string>();

            Assert.Equal(sut.Select(g).Select(f), sut.Select(s => f(g(s))));
        }
    }
}
