﻿namespace TaxCalculator.Core.Errors {
    public static class ErrorCodes {
        public static string StringLengthError = "STR001";

        public static string StringNullOrEmptyError = "STR002";

        public static string StringNotLetters = "STR004";

        public static string StringMustHaveLettersOnlyStrict = "STR005";

        public static string PostCodeNotAmerican = "ZIP001";

        public static string PostCodeNotEuropean = "ZIP002";

        public static string PostCodeNotAustralian = "ZIP003";

        public static string PostCodeNotCanadian = "ZIP004";

        public static string CurrencyNotPositiveAmount = "CUR001";

        public static string CollectionMustBeNullOrEmpty = "COL001";

        public static string CalculationMustNotHaveTwoAmountsSet = "USD002";

        public static string CalculationExemptionTypeNotFound = "USD004";

        public static string IntMustNotBeNegative = "INT001";

        public static string IntParsingFailed = "INT002";

        public static string IntMustBePositive = "INT003";

        public static string DecimalMustNotBeNegative = "DEC001";

        public static string DecimalParsingFailed = "DEC002";

        public static string DecimalMustBeLessThan = "DEC003";

        public static string TaxJarApiUnsupportedCountry = "TJR001";

        public static string LineItemDiscountCannotExceedUnitPrice = "LNI001";

        public static string ToStateNotValidIsoCode = "ISO001";

        public static string FromStateNotValidIsoCode = "ISO002";
    }
}
