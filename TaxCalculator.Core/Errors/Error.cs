﻿using System;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Errors {
    public class Error : IEquatable<Error> {
        public bool Equals(Error other) {
            if (ReferenceEquals(null, other)) {
                return false;
            }

            if (ReferenceEquals(this, other)) {
                return true;
            }

            return string.Equals(Code, other.Code, StringComparison.OrdinalIgnoreCase)
                   && string.Equals(Message, other.Message, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((Error) obj);
        }

        public override int GetHashCode() {
            var hashCode = new HashCode();
            hashCode.Add(Code, StringComparer.OrdinalIgnoreCase);
            hashCode.Add(Message, StringComparer.OrdinalIgnoreCase);
            return hashCode.ToHashCode();
        }

        public static bool operator ==(Error left, Error right) => Equals(left, right);

        public static bool operator !=(Error left, Error right) => !Equals(left, right);

        public readonly string Code;

        public readonly string Message;

        public Error(string code, string message) {
            Code = code.ThrowIfNull(nameof(code));
            Message = message.ThrowIfNull(nameof(message));
        }

        public override string ToString() => $"Error Code {Code}: {Message}";
    }
}
