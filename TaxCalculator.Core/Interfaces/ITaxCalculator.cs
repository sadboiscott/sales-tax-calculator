﻿using TaxCalculator.Core.Errors;
using TaxCalculator.Core.Functors;
using TaxCalculator.Core.Models;

namespace TaxCalculator.Core.Interfaces {
    public interface ITaxCalculator {
        public Either<ErrorList, CalculationResponse> CalculateTaxesOnOrder(CalculationRequest request);

        public Either<ErrorList, LocationTaxesResponse> GetTaxesForLocation(LocationTaxesRequest request);
    }
}
