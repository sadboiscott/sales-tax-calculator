﻿namespace TaxCalculator.Core.Models {
    public class LocationTaxesRequest {
        public Address Address { get; }

        public LocationTaxesRequest(Address address) {
            Address = address;
        }
    }
}
