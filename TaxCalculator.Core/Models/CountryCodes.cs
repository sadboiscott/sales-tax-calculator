﻿using System.Collections.Generic;

namespace TaxCalculator.Core.Models {
    public static class CountryCodes {
        public static string America = "US";

        public static string Australia = "AU";

        public static string Canada = "CA";

        public static string Austria = "AT";

        public static string Belgium = "BE";

        public static string Bulgaria = "BG";

        public static string Croatia = "HR";

        public static string Cyprus = "CY";

        public static string CzechRepublic = "CZ";

        public static string Denmark = "DK";

        public static string Estonia = "EE";

        public static string Finland = "FI";

        public static string France = "FR";

        public static string Germany = "DE";

        public static string Greece = "GR";

        public static string Hungary = "HU";

        public static string Ireland = "IE";

        public static string Italy = "IT";

        public static string Latvia = "LV";

        public static string Lithuania = "LT";

        public static string Luxembourg = "LU";

        public static string Malta = "MT";

        public static string Netherlands = "NL";

        public static string Poland = "PL";

        public static string Portugal = "PT";

        public static string Romania = "RO";

        public static string Slovakia = "SK";

        public static string Slovenia = "SI";

        public static string Spain = "ES";

        public static string Sweden = "SE";

        public static string UnitedKingdom = "GB";

        public static IEnumerable<string> EuropeanCountries = new[] {
                                                                        Austria,
                                                                        Belgium,
                                                                        Bulgaria,
                                                                        Croatia,
                                                                        Cyprus,
                                                                        CzechRepublic,
                                                                        Denmark,
                                                                        Estonia,
                                                                        Finland,
                                                                        France,
                                                                        Germany,
                                                                        Greece,
                                                                        Hungary,
                                                                        Ireland,
                                                                        Italy,
                                                                        Latvia,
                                                                        Lithuania,
                                                                        Luxembourg,
                                                                        Malta,
                                                                        Netherlands,
                                                                        Poland,
                                                                        Portugal,
                                                                        Romania,
                                                                        Slovakia,
                                                                        Slovenia,
                                                                        Spain,
                                                                        Sweden,
                                                                        UnitedKingdom
                                                                    };

    }
}
