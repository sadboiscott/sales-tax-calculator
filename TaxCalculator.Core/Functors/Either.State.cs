﻿using Ardalis.SmartEnum;

namespace TaxCalculator.Core.Functors {
    public class EitherState : SmartEnum<EitherState> {
        public static readonly EitherState Bottom = new EitherState(nameof(Bottom), 0);
        public static readonly EitherState Left = new EitherState(nameof(Left), 1);
        public static readonly EitherState Right = new EitherState(nameof(Right), 2);

        private EitherState(string name, int value) : base(name, value) {

        }
    }
}
