﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Functors {
    public static class Maybe
    {
        public static Maybe<T> None<T>() => new Maybe<T>();

        public static Maybe<T> Some<T>(T value) => new Maybe<T>(value);

        public static Maybe<T> FromObject<T>(T value) => (value == null) switch {
            false => Some(value),
            _ => None<T>()
        };
    }

    public readonly struct Maybe<T> : IEquatable<Maybe<T>>,
        IEquatable<T>,
        ISerializable,
        IEnumerable,
        IEnumerable<T> {
        IEnumerator<T> IEnumerable<T>.GetEnumerator() {
            yield break;
        }

        public override bool Equals(object obj) => obj is Maybe<T> other && Equals(other);

        public override int GetHashCode() => HashCode.Combine(Value, HasValue);

        public static bool operator ==(Maybe<T> left, Maybe<T> right) => left.Equals(right);

        public static bool operator !=(Maybe<T> left, Maybe<T> right) => !left.Equals(right);

        internal readonly T Value;

        internal readonly bool HasValue;

        public Maybe(T value) {
            Value = value.ThrowIfNull();
            HasValue = true;
        }

        public Maybe(SerializationInfo info, StreamingContext context) {
            HasValue = (bool) info.GetValue("HasValue", typeof(bool)).ThrowIfNull();

            Value = HasValue ? (T) info.GetValue("Value", typeof(T)).ThrowIfNull() : default;

        }

        public bool IsSuccess => HasValue;

        public bool IsFailure => !HasValue;

        public bool Equals(Maybe<T> other) {
            return EqualityComparer<T>.Default.Equals(Value, other.Value) && HasValue == other.HasValue;
        }

        public bool Equals(T other) => HasValue switch {
            true => other.Equals(Value),
            false => false
        };

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("HasValue", HasValue);

            if (HasValue) {
                info.AddValue("Value", Value);
            }
        }

        public IEnumerator GetEnumerator() {
            if (IsSuccess) {
                yield return Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public static implicit operator Maybe<T>(T value) => new Maybe<T>(value);

        public static explicit operator T(Maybe<T> value) => value.ThrowIfNull().HasValue
            ? value.Value
            : throw new InvalidCastException(
                "Cannot cast a Maybe<T> to underlying value type T while no value is present");
    }
}
