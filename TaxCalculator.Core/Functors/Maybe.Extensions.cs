﻿using System;
using System.Collections.Generic;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Functors {
    public static class MaybeExtensions {
        public static TResult
            Fold<T, TResult>(this Maybe<T> input, TResult seed, Func<TResult, T, TResult> folder) =>
            input.IsSuccess switch {
                true => folder.ThrowIfNull(nameof(folder))(seed, input.Value),
                _ => seed
            };

        public static TResult BiFold<T, TResult>(this Maybe<T> input, TResult seed,
            Func<TResult, T, TResult> someFolder,
            Func<TResult, TResult> noneFolder) => input.IsSuccess switch {
            true => someFolder.ThrowIfNull(nameof(someFolder))(seed, input.Value),
            _ => noneFolder.ThrowIfNull(nameof(noneFolder))(seed)
        };

        public static bool All<T>(this Maybe<T> input, Predicate<T> predicate) => input.IsSuccess switch {
            true => predicate.ThrowIfNull(nameof(predicate))(input.Value),
            _ => true
        };

        public static bool Any<T>(this Maybe<T> input, Predicate<T> predicate) => input.IsSuccess switch {
            true => predicate.ThrowIfNull(nameof(predicate))(input.Value),
            _ => false
        };

        public static Maybe<TOut> Bind<T, TOut>(this Maybe<T> input, Func<T, Maybe<TOut>> binder) =>
            input.IsSuccess switch {
                true => binder.ThrowIfNull(nameof(binder))(input.Value),
                _ => Maybe.None<TOut>()
            };

        public static Maybe<TOut> BiBind<T, TOut>(this Maybe<T> input, Func<T, Maybe<TOut>> someBinder,
            Func<Maybe<TOut>> noneBinder) =>
            input.IsSuccess switch {
                true => someBinder.ThrowIfNull(nameof(someBinder))(input.Value),
                false => noneBinder.ThrowIfNull(nameof(noneBinder))()
            };

        public static bool Contains<T>(this Maybe<T> input, T value) => input.IsSuccess switch {
            true => input.Value.Equals(value),
            false => false
        };

        public static int Count<T>(this Maybe<T> input) => input.IsSuccess switch {
            true => 1,
            _ => 0
        };

        public static void ForEach<T>(this Maybe<T> input, Action<T> action) {
            if (input.IsSuccess) {
                action.ThrowIfNull(nameof(action))(input.Value);
            }
        }

        public static void BiForEach<T>(this Maybe<T> input, Action<T> someAction, Action noneAction) {
            if (input.IsSuccess) {
                someAction.ThrowIfNull(nameof(someAction))(input.Value);
            }
            else {
                noneAction.ThrowIfNull(nameof(noneAction))();
            }
        }

        public static Maybe<T> MaybeOrElse<T>(this Maybe<T> input, T ifNone) => input.IsSuccess switch {
            true => input,
            _ => Maybe.Some(ifNone)
        };

        public static Maybe<T> MaybeOrElse<T>(this Maybe<T> input, Func<T> ifNoneThunk) => input.IsSuccess switch {
            true => input,
            _ => Maybe.Some(ifNoneThunk.ThrowIfNull(nameof(ifNoneThunk))())
        };

        public static Maybe<TOut> Select<T, TOut>(this Maybe<T> input, Func<T, TOut> mapping) =>
            input.IsSuccess switch {
                true => Maybe.Some(mapping.ThrowIfNull(nameof(mapping))(input.Value)),
                _ => Maybe.None<TOut>()
            };

        public static T SingleOr<T>(this Maybe<T> input, T or) => input.IsSuccess switch {
            true => input.Value,
            _ => or
        };

        public static T SingleOr<T>(this Maybe<T> input, Func<T> orThunk) =>
            input.IsSuccess switch {
                true => input.Value,
                _ => orThunk.ThrowIfNull(nameof(orThunk))()
            };

        public static T[] ToArray<T>(this Maybe<T> input) => input.IsSuccess switch {
            true => new[] {input.Value},
            _ => new T[0]
        };

        public static List<T> ToList<T>(this Maybe<T> input) => input.IsSuccess switch {
            true => new List<T> {input.Value},
            _ => new List<T>()
        };

        public static T? ToNullable<T>(this Maybe<T> input) where T: struct => input.IsSuccess switch {
            true => input.Value,
            _ => null
        };

        public static object ToObject<T>(this Maybe<T> input) => input.IsSuccess switch {
            true => input.Value,
            false => null
        };

        public static Maybe<T> Where<T>(this Maybe<T> input, Predicate<T> predicate) => input.IsSuccess switch {
            true => predicate.ThrowIfNull(nameof(predicate))(input.Value) ? input : Maybe.None<T>(),
            false => Maybe.None<T>()
        };

        public static T GetOrThrow<T, TErr>(this Maybe<T> input, Func<TErr> error) where TErr : Exception =>
            input.IsSuccess switch {
                true => input.Value,
                _ => throw error.ThrowIfNull(nameof(error))()
            };

        public static Maybe<TOut> SelectMany<T, TOut>(this Maybe<T> input, Func<T, Maybe<TOut>> mapping) =>
            input.IsSuccess switch {
                true => mapping.ThrowIfNull(nameof(mapping))(input.Value),
                _ => Maybe.None<TOut>()
            };

        public static Maybe<TFinal> SelectMany<TIn, TMid, TFinal>(this Maybe<TIn> input, Func<TIn, Maybe<TMid>> binder,
            Func<TIn, TMid, TFinal> selector) {
            if (input.IsFailure) {
                return Maybe.None<TFinal>();
            }

            var mb = binder(input.Value);
            return mb.IsFailure
                ? Maybe.None<TFinal>()
                : Maybe.Some(selector.ThrowIfNull(nameof(selector))(input.Value, mb.Value).ThrowIfNull());
        }

        public static Type GetUnderlyingType<T>(this Maybe<T> input) => typeof(T);
    }
}
