﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TaxCalculator.Core.Interfaces;
using TaxCalculator.Core.Models;
using TaxCalculator.Core.Validation;

namespace TaxCalculator.Core.Functors {
    public readonly struct Either<TL, TR> : IEnumerable,
        IEquatable<Either<TL, TR>>,
        ISerializable,
        IEither,
        IEquatable<TR> {
        public override bool Equals(object obj) => obj is Either<TL, TR> other && Equals(other);

        public override int GetHashCode() => HashCode.Combine(Left, Right);

        public static bool operator ==(Either<TL, TR> left, Either<TL, TR> right) => left.Equals(right);

        public static bool operator !=(Either<TL, TR> left, Either<TL, TR> right) => !left.Equals(right);

        public static readonly Either<TL, TR> Bottom = new Either<TL, TR>();

        private readonly EitherState _state;

        internal readonly TL Left;

        internal readonly TR Right;

        public EitherState State => _state ?? EitherState.Bottom;

        internal Either(TR right) {
            Right = right.ThrowIfNull(nameof(right));
            Left = default;
            _state = EitherState.Right;
        }

        internal Either(TL left) {
            Left = left.ThrowIfNull(nameof(left));
            Right = default;
            _state = EitherState.Left;
        }

        private Either(SerializationInfo info, StreamingContext context) {
            _state = (EitherState) info.GetValue("State", typeof(EitherState));

            if (_state.ThrowIfNull(nameof(_state)) == EitherState.Bottom.Value) {
                Left = default;
                Right = default;
            }
            else if (_state.Value == EitherState.Left) {
                Left = (TL) info.GetValue("Left", typeof(TL));
                Right = default;
            }
            else if (_state.Value == EitherState.Right) {
                Left = default;
                Right = (TR) info.GetValue("Right", typeof(TR));
            }
            else {
                throw new InvalidOperationException("State of Either functor was set to a value outside of the allowable states.");
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator GetEnumerator() {
            if (IsRight) {
                yield return Right;
            }
        }

        public bool Equals(Either<TL, TR> other) {
            return EqualityComparer<TL>.Default.Equals(Left, other.Left) && EqualityComparer<TR>.Default.Equals(Right, other.Right);
        }

        public bool Equals(TR other) => EqualityComparer<TR>.Default.Equals(Right, other);

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("State", _state);
            if (IsRight) { info.AddValue("Right", Right); }
            if (IsLeft) { info.AddValue("Left", Left); }
        }

        public bool IsRight => State == EitherState.Right;

        public bool IsLeft => State == EitherState.Left;

        public bool IsBottom => State == EitherState.Bottom;

        public TResult MatchUntyped<TResult>(Func<object, TResult> right, Func<object, TResult> left) => default;

        public Type GetUnderlyingLeftType() => typeof(TL);

        public Type GetUnderlyingRightType() => typeof(TR);

        public static explicit operator TR(Either<TL, TR> value) => value.ThrowIfNull().IsRight
            ? value.Right
            : throw new InvalidCastException("Either is not in the Right state.");

        public static explicit operator TL(Either<TL, TR> value) => value.ThrowIfNull().IsLeft
            ? value.Left
            : throw new InvalidCastException("Either is not in the Left state.");

        public static implicit operator Either<TL, TR>(TR value) =>
            new Either<TL, TR>(value);

        public static implicit operator Either<TL, TR>(TL value) =>
            new Either<TL, TR>(value);

        public override string ToString() => IsBottom
            ? "Bottom"
            : IsRight
                ? $"Right({Right})"
                : $"Left({Left})";
    }
}
