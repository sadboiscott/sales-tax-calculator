﻿using System.Collections.Generic;
using System.Linq;
using TaxCalculator.Core.Models;

namespace TaxCalculator.Core.Functors {
    public static class MaybeEnumerableExtensions {
        public static Maybe<T> Head<T>(this IEnumerable<T> input) {
            foreach (var x in input) {
                return Maybe.Some(x);
            }

            return Maybe.None<T>();
        }

        public static IEnumerable<T> Cons<T>(this IEnumerable<T> input) {
            if (!input.Skip(1).Any()) {
                yield break;
            }

            foreach (var x in input.Skip(1)) {
                yield return x;
            }
        }
    }
}
